-------------------------------------------------------------------
--									--
--									--
--NPCScript06.lua Created by knight.gong 2005.7.12.				--
--									--
--									--
--------------------------------------------------------------------------
print( "loading NPCScript06.lua" )

function KZ_xyq()
	Talk(1,"Don't look down on me judging from my size! I'm way stronger than you think��")
	
	Text(1,"Exchange for Apparel",JumpPage, 27)
	--Text(1,"�һ�����ͼֽC",JumpPage, 11)
	--Text(1,"�һ�����ͼֽD",JumpPage, 12)
	Text(1,"Exchange for Constellation Medal",JumpPage, 13)

	Talk(27,"Please select apparel choice")	

	Text(27,"Leo Apparel",JumpPage, 2)
	Text(27,"Virgo Apparel",JumpPage, 3)
	Text(27,"Libra Apparel",JumpPage, 4)
	Text(27,"Scorpio Apparel",JumpPage, 5)
	Text(27,"Sagittarius Apparel",JumpPage, 6)
	Text(27,"Capricorn Apparel",JumpPage, 7)
	Text(27,"Aquarius Apparel",JumpPage, 8)
	Text(27,"Pisces Apparel",JumpPage, 9)


	Talk(2,"Leo Essence 1,Leo Energy 1,Leo Chest 1,Leo Key 1")
	
	InitTrigger()
	TriggerCondition( 1, HasItem, 8714, 1 )	
	TriggerCondition( 1, HasItem, 8715, 1 )	
	TriggerCondition( 1, HasItem, 8716, 1 )	
	TriggerCondition( 1, HasItem, 8445, 1 )	
	TriggerCondition(1, HasLeaveBagGrid, 1)
	TriggerCondition(1, KitbagLock, 0 )
    	TriggerAction( 1, TakeItem, 8714, 1 )	
   	TriggerAction( 1, TakeItem, 8715, 1 )
    	TriggerAction( 1, TakeItem, 8716, 1 )	
   	TriggerAction( 1, TakeItem, 8445, 1 )	
	TriggerAction( 1, GiveItem,8648, 1,4 )	
	TriggerFailure( 1, JumpPage, 10 ) 					
	Text(2,"Exchange",MultiTrigger, GetMultiTrigger(), 1)

	Talk(3,"Virgo Essence 1,Virgo Energy 1,Virgo Chest 1,Virgo Key 1")
	
	InitTrigger()
	TriggerCondition( 1, HasItem, 8446, 1 )	
	TriggerCondition( 1, HasItem, 8447, 1 )	
	TriggerCondition( 1, HasItem, 8448, 1 )	
	TriggerCondition( 1, HasItem, 8449, 1 )	
	TriggerCondition(1, HasLeaveBagGrid, 1)
	TriggerCondition(1, KitbagLock, 0 )
    	TriggerAction( 1, TakeItem, 8446, 1 )	
   	TriggerAction( 1, TakeItem, 8447, 1 )
    	TriggerAction( 1, TakeItem, 8448, 1 )	
   	TriggerAction( 1, TakeItem, 8449, 1 )	
	TriggerAction( 1, GiveItem,8722, 1,4 )	
	TriggerFailure( 1, JumpPage, 10 ) 					
	Text(3,"Exchange",MultiTrigger, GetMultiTrigger(), 1)


	Talk(4,"Libra Essence 1,Libra Energy 1,Libra Chest 1,Libra Key 1")
	
	InitTrigger()
	TriggerCondition( 1, HasItem, 8450, 1 )	
	TriggerCondition( 1, HasItem, 8451, 1 )	
	TriggerCondition( 1, HasItem, 8452, 1 )	
	TriggerCondition( 1, HasItem, 8453, 1 )	
	TriggerCondition(1, HasLeaveBagGrid, 1)
	TriggerCondition(1, KitbagLock, 0 )
    	TriggerAction( 1, TakeItem, 8450, 1 )	
   	TriggerAction( 1, TakeItem, 8451, 1 )
    	TriggerAction( 1, TakeItem, 8452, 1 )	
   	TriggerAction( 1, TakeItem, 8453, 1 )	
	TriggerAction( 1, GiveItem,8723, 1,4 )	
	TriggerFailure( 1, JumpPage, 10 ) 					
	Text(4,"Exchange",MultiTrigger, GetMultiTrigger(), 1)



	Talk(5,"Scorpio Essence 1,Scorpio Energy 1,Scorpio Chest 1,Scorpio Key 1")
	
	InitTrigger()
	TriggerCondition( 1, HasItem, 8454, 1 )	
	TriggerCondition( 1, HasItem, 8455, 1 )	
	TriggerCondition( 1, HasItem, 8456, 1 )	
	TriggerCondition( 1, HasItem, 8457, 1 )	
	TriggerCondition(1, HasLeaveBagGrid, 1)
	TriggerCondition(1, KitbagLock, 0 )
    	TriggerAction( 1, TakeItem, 8454, 1 )	
   	TriggerAction( 1, TakeItem, 8455, 1 )
    	TriggerAction( 1, TakeItem, 8456, 1 )	
   	TriggerAction( 1, TakeItem, 8457, 1 )	
	TriggerAction( 1, GiveItem,8724, 1,4 )	
	TriggerFailure( 1, JumpPage, 10 ) 					
	Text(5,"Exchange",MultiTrigger, GetMultiTrigger(), 1)


	Talk(6,"Sagittarius Essence 1,Sagittarius Energy 1,Sagittarius Chest 1,Sagittarius Key 1")
	
	InitTrigger()
	TriggerCondition( 1, HasItem, 8458, 1 )	
	TriggerCondition( 1, HasItem, 8459, 1 )	
	TriggerCondition( 1, HasItem, 8460, 1 )	
	TriggerCondition( 1, HasItem, 8461, 1 )	
	TriggerCondition(1, HasLeaveBagGrid, 1)
	TriggerCondition(1, KitbagLock, 0 )
    	TriggerAction( 1, TakeItem, 8458, 1 )	
   	TriggerAction( 1, TakeItem, 8459, 1 )
    	TriggerAction( 1, TakeItem, 8460, 1 )	
   	TriggerAction( 1, TakeItem, 8461, 1 )	
	TriggerAction( 1, GiveItem,8725, 1,4 )	
	TriggerFailure( 1, JumpPage, 10 ) 					
	Text(6,"Exchange",MultiTrigger, GetMultiTrigger(), 1)


	Talk(7,"Capricorn Essence 1,Capricorn Energy 1,Capricorn Chest 1,Capricorn Key 1")
	
	InitTrigger()
	TriggerCondition( 1, HasItem, 8462, 1 )	
	TriggerCondition( 1, HasItem, 8463, 1 )	
	TriggerCondition( 1, HasItem, 8464, 1 )	
	TriggerCondition( 1, HasItem, 8465, 1 )	
	TriggerCondition(1, HasLeaveBagGrid, 1)
	TriggerCondition(1, KitbagLock, 0 )
    	TriggerAction( 1, TakeItem, 8462, 1 )	
   	TriggerAction( 1, TakeItem, 8463, 1 )
    	TriggerAction( 1, TakeItem, 8464, 1 )	
   	TriggerAction( 1, TakeItem, 8465, 1 )	
	TriggerAction( 1, GiveItem,8649, 1,4 )	
	TriggerFailure( 1, JumpPage, 10 ) 					
	Text(7,"Exchange",MultiTrigger, GetMultiTrigger(), 1)


	Talk(8,"Aquarius Essence 1,Aquarius Energy 1,Aquarius Chest 1,Aquarius Key 1")
	
	InitTrigger()
	TriggerCondition( 1, HasItem, 8466, 1 )	
	TriggerCondition( 1, HasItem, 8467, 1 )	
	TriggerCondition( 1, HasItem, 8468, 1 )	
	TriggerCondition( 1, HasItem, 8469, 1 )	
	TriggerCondition(1, HasLeaveBagGrid, 1)
	TriggerCondition(1, KitbagLock, 0 )
    	TriggerAction( 1, TakeItem, 8466, 1 )	
   	TriggerAction( 1, TakeItem, 8467, 1 )
    	TriggerAction( 1, TakeItem, 8468, 1 )	
   	TriggerAction( 1, TakeItem, 8469, 1 )	
	TriggerAction( 1, GiveItem,8650, 1,4 )	
	TriggerFailure( 1, JumpPage, 10 ) 					
	Text(8,"Exchange",MultiTrigger, GetMultiTrigger(), 1)

	Talk(9,"Pisces Essence 1,Pisces Energy 1,Pisces Chest 1,Pisces Key 1")
	
	InitTrigger()
	TriggerCondition( 1, HasItem, 8470, 1 )	
	TriggerCondition( 1, HasItem, 8471, 1 )	
	TriggerCondition( 1, HasItem, 8472, 1 )	
	TriggerCondition( 1, HasItem, 8473, 1 )	
	TriggerCondition(1, HasLeaveBagGrid, 1)
	TriggerCondition(1, KitbagLock, 0 )
    	TriggerAction( 1, TakeItem, 8470, 1 )	
   	TriggerAction( 1, TakeItem, 8471, 1 )
    	TriggerAction( 1, TakeItem, 8472, 1 )	
   	TriggerAction( 1, TakeItem, 8473, 1 )	
	TriggerAction( 1, GiveItem,8651, 1,4 )	
	TriggerFailure( 1, JumpPage, 10 ) 					
	Text(9,"Exchange",MultiTrigger, GetMultiTrigger(), 1)

	Talk( 10, "Exchange requirement insufficient otherwise your inventory is locked. Please ensure you have at least 1 empty slot.")
	
	Talk( 11, "Redeem Abandon Print C requires 50 Abandon Print A or 40 Abandon Print B, inclusive of a couple of Commerce Permit")
	
	Text( 11,"Exchange using 50 Abandon Print A with a couple of Commerce Permit",JumpPage, 14)
	Text( 11,"Exchange using 40 Abandon Print B with a couple of Commerce Permit",JumpPage, 15)

	Talk( 14, "Select what Permit for exchange.")

	Text( 14,"Exchange using 50 Abandon Print A with 20 Junior Commerce Permit",JumpPage, 16)
	Text( 14,"Exchange using 50 Abandon Print A with 15 Intermediate Commerce Permit for Abandon Print C. Confirm?",JumpPage, 17)
	Text( 14,"Exchange using 50 Abandon Print A with 10 Advance Commerce Permit for Abandon Print C. Confirm?",JumpPage, 18)

	Talk( 16, "Exchange using 50 Abandon Print A with 20 Junior Commerce Permit for Abandon Print C. Confirm��")
	
	InitTrigger()
	TriggerCondition( 1, HasItem, 1000, 50 )	
	TriggerCondition( 1, HasItem, 8318, 20 )		
	TriggerCondition(1, HasLeaveBagGrid, 1)
	TriggerCondition(1, KitbagLock, 0 )
    	TriggerAction( 1, TakeItem, 1000, 50 )	
   	TriggerAction( 1, TakeItem, 8318, 20 )	
	TriggerAction( 1, GiveItem,1002, 1,4 )	
	TriggerFailure( 1, JumpPage, 10 ) 					
	Text( 16,"Exchange",MultiTrigger, GetMultiTrigger(), 1)

	Talk( 17, "Exchange using 50 Abandon Print A with 15 Intermediate Commerce Permit for Abandon Print C. Confirm?")
	
	InitTrigger()
	TriggerCondition( 1, HasItem, 1000, 50 )	
	TriggerCondition( 1, HasItem, 8319, 15 )		
	TriggerCondition(1, HasLeaveBagGrid, 1)
	TriggerCondition(1, KitbagLock, 0 )
    	TriggerAction( 1, TakeItem, 1000, 50 )	
   	TriggerAction( 1, TakeItem, 8319, 15 )	
	TriggerAction( 1, GiveItem,1002, 1,4 )	
	TriggerFailure( 1, JumpPage, 10 ) 					
	Text( 17,"Exchange",MultiTrigger, GetMultiTrigger(), 1)
	
	
	Talk( 18, "Exchange using 50 Abandon Print A with 10 Advance Commerce Permit for Abandon Print C. Confirm?")
	
	InitTrigger()
	TriggerCondition( 1, HasItem, 1000, 50 )	
	TriggerCondition( 1, HasItem, 8320, 10 )		
	TriggerCondition(1, HasLeaveBagGrid, 1)
	TriggerCondition(1, KitbagLock, 0 )
    	TriggerAction( 1, TakeItem, 1000, 50 )	
   	TriggerAction( 1, TakeItem, 8320, 10 )	
	TriggerAction( 1, GiveItem,1002, 1,4 )	
	TriggerFailure( 1, JumpPage, 10 ) 					
	Text( 18,"Exchange",MultiTrigger, GetMultiTrigger(), 1)

	Talk( 15, "Select what Permit for exchange.")

	Text( 15,"Exchange using 40 Abandon Print B with 20 Junior Commerce Permit",JumpPage, 19)
	Text( 15,"Exchange using 40 Abandon Print B with 15 Intermediate Commerce Permit",JumpPage, 20)
	Text( 15,"Exchange using 40 Abandon Print B with 10 Advance Commerce Permit",JumpPage, 21)

	Talk( 19, "Exchange using 50 Abandon Print A with 20 Junior Commerce Permit for Abandon Print C. Confirm?")
	
	InitTrigger()
	TriggerCondition( 1, HasItem, 1001, 40 )	
	TriggerCondition( 1, HasItem, 8318, 20 )		
	TriggerCondition(1, HasLeaveBagGrid, 1)
	TriggerCondition(1, KitbagLock, 0 )
    	TriggerAction( 1, TakeItem, 1001, 40 )	
   	TriggerAction( 1, TakeItem, 8318, 20 )	
	TriggerAction( 1, GiveItem,1002, 1,4 )	
	TriggerFailure( 1, JumpPage, 10 ) 					
	Text( 19,"Exchange",MultiTrigger, GetMultiTrigger(), 1)

	Talk( 20, "Exchange using 50 Abandon Print A with 15 Intermediate Commerce Permit for Abandon Print C. Confirm?")
	
	InitTrigger()
	TriggerCondition( 1, HasItem, 1001, 40 )	
	TriggerCondition( 1, HasItem, 8319, 15 )		
	TriggerCondition(1, HasLeaveBagGrid, 1)
	TriggerCondition(1, KitbagLock, 0 )
    	TriggerAction( 1, TakeItem, 1001, 40 )	
   	TriggerAction( 1, TakeItem, 8319, 15 )	
	TriggerAction( 1, GiveItem,1002, 1,4 )	
	TriggerFailure( 1, JumpPage, 10 ) 					
	Text( 20,"Exchange",MultiTrigger, GetMultiTrigger(), 1)
	
	
	Talk( 21, "Exchange using 50 Abandon Print A with 10 Advance Commerce Permit for Abandon Print C. Confirm?")
	
	InitTrigger()
	TriggerCondition( 1, HasItem, 1001, 40 )	
	TriggerCondition( 1, HasItem, 8320, 10 )		
	TriggerCondition(1, HasLeaveBagGrid, 1)
	TriggerCondition(1, KitbagLock, 0 )
    	TriggerAction( 1, TakeItem, 1001, 40 )	
   	TriggerAction( 1, TakeItem, 8320, 10 )	
	TriggerAction( 1, GiveItem,1002, 1,4 )	
	TriggerFailure( 1, JumpPage, 10 ) 					
	Text( 21,"Exchange",MultiTrigger, GetMultiTrigger(), 1)

	Talk( 12, "Redeem Abandon Print D requires 15 Abandon Print C , inclusive of a couple of Commerce Permit")

	Text( 12,"Exchange using 15 Abandon Print C with 100 Intermediate Commerce Permit",JumpPage, 22)
	Text( 12,"Exchange using 15 Abandon Print C with 50 Advance Commerce Permit",JumpPage, 23)

	
	Talk( 22, "Exchange using 15 Abandon Print C with 100 Intermediate Commerce Permit for Abandon Print D. Confirm?")
	
	InitTrigger()
	TriggerCondition( 1, HasItem, 1002, 15 )	
	TriggerCondition( 1, HasItem, 8319, 100 )		
	TriggerCondition(1, HasLeaveBagGrid, 1)
	TriggerCondition(1, KitbagLock, 0 )
    	TriggerAction( 1, TakeItem, 1002, 15 )	
   	TriggerAction( 1, TakeItem, 8319, 100 )	
	TriggerAction( 1, GiveItem,1003, 1,4 )	
	TriggerFailure( 1, JumpPage, 10 ) 					
	Text( 22,"Exchange",MultiTrigger, GetMultiTrigger(), 1)

	Talk( 23, "Exchange using 15 Abandon Print C with 50 Advance Commerce Permit for Abandon Print D. Confirm?")
	
	InitTrigger()
	TriggerCondition( 1, HasItem, 1002, 15 )	
	TriggerCondition( 1, HasItem, 8320, 50 )		
	TriggerCondition(1, HasLeaveBagGrid, 1)
	TriggerCondition(1, KitbagLock, 0 )
    TriggerAction( 1, TakeItem, 1002, 15 )	
   	TriggerAction( 1, TakeItem, 8320, 50 )	
	TriggerAction( 1, GiveItem,1003, 1,4 )	
	TriggerFailure( 1, JumpPage, 10 ) 					
	Text( 23,"Exchange",MultiTrigger, GetMultiTrigger(), 1)

	Talk(13,"Exchange for Constellation Medal requires a couple of Commerce Permit~")
	
	Text( 13,"Exchange using 40 Junior Commerce Permit",JumpPage, 24)
	Text( 13,"Exchange using 20 Intermediate Commerce Permit",JumpPage, 25)
	Text( 13,"Exchange using 5 Advance Commerce Permit",JumpPage, 26)

	Talk( 24, "Exchange using 40 Junior Commerce Permit for Constellation Medal. Confirm?")
	
	InitTrigger()
	TriggerCondition( 1, HasItem, 8318, 40 )
	TriggerCondition( 1, HasMoney, 100000 )	
	TriggerCondition(1, HasLeaveBagGrid, 1)
	TriggerCondition(1, KitbagLock, 0 )
    TriggerAction( 1, TakeItem, 8318, 40 )
	TriggerAction( 1, TakeMoney, 100000 )	
	TriggerAction( 1, GiveItem,8713, 1,4 )	
	TriggerFailure( 1, JumpPage, 10 ) 					
	Text( 24,"Exchange",MultiTrigger, GetMultiTrigger(), 1)


	Talk( 25, "Exchange using 20 Intermediate Commerce Permit for Constellation Medal. Confirm?")
	
	InitTrigger()
	TriggerCondition( 1, HasItem, 8319, 20 )	
	TriggerCondition( 1, HasMoney, 100000 )		
	TriggerCondition(1, HasLeaveBagGrid, 1)
	TriggerCondition(1, KitbagLock, 0 )
    TriggerAction( 1, TakeItem, 8319, 20 )	
	TriggerAction( 1, TakeMoney, 100000 )
	TriggerAction( 1, GiveItem,8713, 1,4 )	
	TriggerFailure( 1, JumpPage, 10 ) 					
	Text( 25,"Exchange",MultiTrigger, GetMultiTrigger(), 1)

	
	Talk( 26, "Exchange using 5 Advance Commerce Permit for Constellation Medal. Confirm?")
	
	InitTrigger()
	TriggerCondition( 1, HasItem, 8320, 5 )
	TriggerCondition( 1, HasMoney, 100000 )		
	TriggerCondition(1, HasLeaveBagGrid, 1)
	TriggerCondition(1, KitbagLock, 0 )
    TriggerAction( 1, TakeItem, 8320, 5 )
	TriggerAction( 1, TakeMoney, 100000 )			
	TriggerAction( 1, GiveItem,8713, 1,4 )	
	TriggerFailure( 1, JumpPage, 10 ) 					
	Text( 26,"Exchange",MultiTrigger, GetMultiTrigger(), 1)

end
