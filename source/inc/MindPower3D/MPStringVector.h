#ifndef MPStringVector_H
#define MPStringVector_H

#include "MPEffPrerequisites.h"

using MPStringVector = std::vector<std::string>;
#endif