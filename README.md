# top-recode

Objective is to recode TOP/PKO .lua files in a way the syntax becomes easier to use, or that implements features that can provide more freedom of coding.

This is only intended for testing. Everything here is meant to be used in your own computer (no website for example): including the server and client.

PKO 2.7 mod is used. My default mod is still on alpha and is not supposed to be used yet.

## Goals (ATM):
- Get it working without major bugs. It is.
- Fix warnings.
- Fix some bugs.
- More later.

## WINDOWS:

### How to install:
1. Install Microsoft Visual Studio 2019 Community Edition. Then on Visual Studio Installer get: VC C++ 2019 v142 toolset, C++ Profiling tools, Windows 10 SDK (latest) for Desktop C++, Visual C++ ATL support.
2. Install and configure MSQL of any version liked - recommended MSQL 2019 and the guide as follows: https://pko.coffeecup.com/. Don't do anything regarding db accounts (except sa), dbs, ip or more yet - leave that part for later.
3. Install git for windows. Select the cmd.exe options (but no options with added unix tools please). Then clone:
4. (using cmd.exe) cd to the folder where you want the project to be, and type:
git clone "https://gitlab.com/deguix/top-recode"
5. Run with Admin priviledges the "build.bat" in the main folder (symlink creation requires admin priviledges) with the following options if needed:
   5.1. "--debug": Use this if doing a debug build (luajit debug files replaces the release ones!)
   5.2. "--x64": Use this for the x64 build. Everything compiles and runs, except for the game client at this moment.
6. (if using SQL 2000) In all .cfg files in server folder, remove any ",1433" you see.
7. (using cmd.exe again) cd to the sql_scripts folder, and run build_db.bat. Now MSQL is completely configured for the project.

### How to update without any other actions (if for some reason build.bat fails and online git files are newer):
1. (using cmd.exe) cd to the project folder, and type:
git pull

### How to update and then compile src files (no symlinks are created here):
1. (using cmd.exe) cd to project folder, and type (if using SQL2000: add --mssql2000 as well):
build.bat --pull

### How to use (assumes build.bat completes successfully once):
1. Start the SQL server (if it isn't by now) and start runall.bat in server folder to start all game server exe's (not applicable to TradeServer and TradeTools yet, they're still being fixed).
2. Run run.bat in client folder to run client.
3. Select the only server available, and put the user name *deguix* and pass *123456* and press Enter (it's a fresh account, no chars are there). The game should be working at this point.

## LINUX:

Copy or symlink folder from Windows. Unfortunatelly the source can only be compiled on Windows.

## Supplemental - For developers only: How to adapt mod files to use this source:
### Server lua files:
1. Make sure the scripts work on TOP2 server.
2. Copy scripts folder from the resource folder of your mod, and paste in inside this projects resource/scripts folder.
3. Rename it to whatever mod name you would like.
4. Copy initial.lua from pko_2.7 (it's a compability mod) to your recently copied mod folder.
5. Create a "maps" folder in your mod folder.
6. Create a folder for each map with its name in the "maps" folder.
7. Copy each map folder .lua files in your original files to respective map folders (that are in your "map folder).
8. (TO BE CHANGED - TODO: Give each mod independence) Copy the .txt and other files (not .lua) of your map folders to the respective map folders in the resource folder.
9. Replace all "local count = ReadByte( rpk )" with local count = ReadWord( rpk )" in ncpsdk.lua (higher item buy/sale limits).

### Client files (TO BE CHANGED - TODO: Give each mod independence):
1. Lua files have no compatibility yet (and no missions/gems yet).
2. Replace table folder of these files with your own mod's.
3. Replace any other files except lua files and system folder files with your own.

### Publishing:
1. When zipping the files, the symlinks will resolve.